# My CodinGame solutions
## Мои решения CodinGame

#### Wondev Woman
[WonDev_Woman-3WL.py](WondevWoman/WonDev_Woman-3WL.py) - проходит третью деревяную лигу
* читает возможные ходы
* делает первый из списка разрешённых ходов

[WonDev_Woman-2WL.py](WondevWoman/WonDev_Woman-2WL.py) - проходит вторую деревяную лигу
* читает возможные ходы
* читает уровень ячеек в доступных ходах
* выбирает "лучшую" ячейку - ячейку с наивысшим уровнем