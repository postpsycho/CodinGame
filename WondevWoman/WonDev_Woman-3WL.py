import sys
import math

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.

# Здесь задаётся размер поля
size = int(input())
# Здесь - количество фишек на игровом поле для каждого игрока.
# В третьей деревяной лиге количество фишек всегда 1.
units_per_player = int(input())

# game loop
while True:
    # Получаем игровое поле
    for i in range(size):
        row = input()
        # print(row, file=sys.stderr)
    # Получаем расположение своих фишек на игровом поле
    for i in range(units_per_player):
        unit_x, unit_y = [int(j) for j in input().split()]
        # print(unit_x, unit_y, file=sys.stderr)
    # Получаем расположение фишек противника на игровом поле
    for i in range(units_per_player):
        other_x, other_y = [int(j) for j in input().split()]
    # Для прохождения этой лиги мы хотим сделать простое - чтобы наша фишка ходила в разные стороны.
    legal_actions_num = int(input())
    # Создаём пустой список, куда будем складывать все возможные ходы
    legal_actions = []
    # Получаем список возможных ходов
    for i in range(legal_actions_num):
        a = input()
        # Добавляем полученные возможные ходы в наш пустой список
        legal_actions.append(a)
        atype, index, dir_1, dir_2 = a.split()
        index = int(index)

    # Write an action using print
    # To debug: print("Debug messages...", file=sys.stderr)

    # Запускаем нашу фишку. Она будет идти по первому возможному ходу.
    print(legal_actions[0])
