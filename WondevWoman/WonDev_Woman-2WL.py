import sys

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.

size = int(input())
units_per_player = int(input())

''' Код ниже будет выбирать из всех доступных ходов тот, который наиболее удачный. 
Удачным считается ход на ячейку с наивысшим уровнем. 
В комментариях здесь буду описывать только дополнения, все предыдущие можно посмотреть постом ранее.'''

# Для начала определяем уровень ячейки. На входе у нас есть строки нашего игрового поля.
# Одна строка представляет собой следующее: '.0000.' (.) - это ячейка, на которую нельзя ходить. Уровни ячеек от 0 до 3.


def level_of(rows, x, y):
    # Ищем ячейку по координате y
    row = rows[y]
    # Получаем уровень нужной нам ячейки
    level = int(row[x])
    return level


# Дальше мы определяем направление движения
def move_estimate(rows, move_dir, unit_x, unit_y):
    if move_dir[0] == 'N':
        new_x = unit_x
        new_y = unit_y - 1
    elif move_dir[0] == 'S':
        new_x = unit_x
        new_y = unit_y + 1
    elif move_dir[0] == 'W':
        new_x = unit_x - 1
        new_y = unit_y
    elif move_dir[0] == 'E':
        new_x = unit_x + 1
        new_y = unit_y
    elif move_dir[0] == 'NW':
        new_x = unit_x - 1
        new_y = unit_y - 1
    elif move_dir[0] == 'NE':
        new_x = unit_x + 1
        new_y = unit_y - 1
    elif move_dir[0] == 'SW':
        new_x = unit_x - 1
        new_y = unit_y + 1
    elif move_dir[0] == 'SE':
        new_x = unit_x + 1
        new_y = unit_y + 1
    level = level_of(rows, new_x, new_y)
    return level
# game loop
while True:
    rows = []
    for i in range(size):
        row = input()
        rows.append(row)
    print('rows=', rows, file=sys.stderr)
    # print(row, file=sys.stderr)
    unit_x = 0
    unit_y = 0
    for i in range(units_per_player):
        unit_x, unit_y = [int(j) for j in input().split()]
        print(unit_x, unit_y, file=sys.stderr)
    for i in range(units_per_player):
        other_x, other_y = [int(j) for j in input().split()]
    legal_actions_num = int(input())
    legal_actions = []
    move_dirs = []
    for i in range(legal_actions_num):
        a = input()
        legal_actions.append(a)
        atype, index, dir_1, dir_2 = a.split()
        move_dirs.append((dir_1, dir_2))
        index = int(index)
    print(legal_actions, file=sys.stderr)

    # Write an action using print
    # To debug: print("Debug messages...", file=sys.stderr)

    moves_num = len(legal_actions) - 1
    if moves_num == 0:
         print('ACCEPT-DEFEAT')

    # Выбор рандомного хода из доступных
# import random
    # rand = random.randint(0, moves_num)
    # print(legal_actions[rand])

    # Выбираем лучшую ячейку
    best_move_dir = move_dirs[0]
    best_move_est = move_estimate(rows, best_move_dir, unit_x, unit_y)

    # Перебираем все доступные ходы. Сравниваем доступные ходы, выбираем тот, чей уровень окажется наиболее высоким
    for current_dir in move_dirs:
        estimate = move_estimate(rows, current_dir, unit_x, unit_y)
        if estimate > best_move_est:
            best_move_est = estimate
            best_move_dir = current_dir
    print('MOVE&BUILD 0', best_move_dir[0],best_move_dir[1])
